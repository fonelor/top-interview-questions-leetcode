package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class IntersectionOfTwoArraysIITest {

    @Test
    void testCase1() {
        int[] intersection = IntersectionOfTwoArraysII.intersect(
                new int[]{1, 2, 2, 1},
                new int[]{2, 2}
        );

        assertThat(intersection).hasSize(2);
        assertThat(intersection).contains(2, 2);
    }

    @Test
    void testCase2() {
        int[] intersection = IntersectionOfTwoArraysII.intersect(
                new int[]{4, 9, 5},
                new int[]{9, 4, 9, 8, 4}
        );

        assertThat(intersection).hasSize(2);
        assertThat(intersection).contains(4, 9);
    }

    @Test
    void testCase3() {
        int[] intersection = IntersectionOfTwoArraysII.intersect(
                new int[]{},
                new int[]{9, 4, 9, 8, 4}
        );

        assertThat(intersection).isEmpty();
    }

    @Test
    void testCase4() {
        int[] intersection = IntersectionOfTwoArraysII.intersect(
                new int[]{0, 0, 0, 0, 0},
                new int[]{9, 4, 9, 0, 8, 4}
        );

        assertThat(intersection).hasSize(1);
        assertThat(intersection).contains(0);
    }
}