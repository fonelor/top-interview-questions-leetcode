package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BestTimeToBuyAndSellStockIITest {

    @Test
    void maxProfit() {
        int[] prices = {7, 1, 5, 3, 6, 4};
        int maxProfit = BestTimeToBuyAndSellStockII.maxProfit(prices);

        assertEquals(7, maxProfit);
    }

    @Test
    void maxProfit2() {
        int[] prices = {1, 2, 3, 4, 5};
        int maxProfit = BestTimeToBuyAndSellStockII.maxProfit(prices);

        assertEquals(4, maxProfit);
    }

    @Test
    void maxProfit3() {
        int[] prices = {7, 6, 4, 3, 1};
        int maxProfit = BestTimeToBuyAndSellStockII.maxProfit(prices);

        assertEquals(0, maxProfit);
    }

    @Test
    void maxProfit4() {
        int[] prices = {1, 2, 3, 3, 1, 2, 4, 4};
        int maxProfit = BestTimeToBuyAndSellStockII.maxProfit(prices);

        assertEquals(5, maxProfit);
    }
}