package ru.rolsoft.leetcode.array;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class MoveZeroesTest {

    private static Stream<Arguments> cases() {
        return Stream.of(
                Arguments.of(new int[]{0, 1, 0, 3, 12}, new int[]{1, 3, 12, 0, 0}),
                Arguments.of(new int[]{0}, new int[]{0}),
                Arguments.of(new int[]{1}, new int[]{1}),
                Arguments.of(new int[]{0, 1}, new int[]{1, 0})
        );
    }


    @MethodSource("cases")
    @ParameterizedTest
    void testCase1(int[] nums, int[] result) {
        MoveZeroes.moveZeroes(nums);

        assertThat(nums).isEqualTo(result);
    }
}