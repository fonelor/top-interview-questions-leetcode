package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RemoveDuplicatesTest {

    @Test
    void removeDuplicates() {
        int[] expected = {1, 2};
        int[] nums = {1, 1, 2};

        int k = RemoveDuplicates.removeDuplicates(nums);

        assertEquals(2, k);
        for (int i = 0; i < k; i++) {
            assertEquals(nums[i], expected[i]);
        }
    }

    @Test
    void removeDuplicates2() {
        int[] expected = {1, 2, 3};
        int[] nums = {1, 1, 2, 2, 3};

        int k = RemoveDuplicates.removeDuplicates(nums);

        assertEquals(3, k);
        for (int i = 0; i < k; i++) {
            assertEquals(nums[i], expected[i]);
        }
    }

    @Test
    void removeDuplicates3() {
        int[] expected = {0, 2, 3};
        int[] nums = {0, 2, 2, 2, 3};

        int k = RemoveDuplicates.removeDuplicates(nums);

        assertEquals(3, k);
        for (int i = 0; i < k; i++) {
            assertEquals(nums[i], expected[i]);
        }
    }

    @Test
    void removeDuplicates4() {
        int[] expected = {};
        int[] nums = {};

        int k = RemoveDuplicates.removeDuplicates(nums);

        assertEquals(0, k);
        for (int i = 0; i < k; i++) {
            assertEquals(nums[i], expected[i]);
        }
    }
}