package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ContainsDuplicateTest {

    @Test
    void testCase1() {
        Assertions.assertTrue(ContainsDuplicate.containsDuplicateSet(new int[]{1, 2, 3, 1}));
    }

    @Test
    void testCase2() {
        Assertions.assertFalse(ContainsDuplicate.containsDuplicateSet(new int[]{1, 2, 3, 4}));
    }

    @Test
    void testCase3() {
        Assertions.assertTrue(ContainsDuplicate.containsDuplicateSet(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}));
    }

    @Test
    void testCaseArray1() {
        Assertions.assertTrue(ContainsDuplicate.containsDuplicateArray(new int[]{1, 2, 3, 1}));
    }

    @Test
    void testCaseArray2() {
        Assertions.assertFalse(ContainsDuplicate.containsDuplicateArray(new int[]{1, 2, 3, 4}));
    }

    @Test
    void testCaseArray3() {
        Assertions.assertTrue(ContainsDuplicate.containsDuplicateArray(new int[]{1, 1, 1, 3, 3, 4, 3, 2, 4, 2}));
    }
}