package ru.rolsoft.leetcode.array;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;


class PlusOneTest {

    private static Stream<Arguments> cases() {
        return Stream.of(
                Arguments.of(new int[]{1, 2, 3}, new int[]{1, 2, 4}),
                Arguments.of(new int[]{4, 3, 2, 1}, new int[]{4, 3, 2, 2}),
                Arguments.of(new int[]{0}, new int[]{1}),
                Arguments.of(new int[]{9}, new int[]{1, 0})
        );
    }

    @ParameterizedTest
    @MethodSource("cases")
    void testCase1(int[] digits, int[] expected) {
        int[] result = PlusOne.plusOne(digits);
        assertThat(result).isEqualTo(expected);
    }
}