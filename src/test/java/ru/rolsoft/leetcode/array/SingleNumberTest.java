package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SingleNumberTest {

    @Test
    void testCase1() {
        // length = 3

        assertEquals(1, SingleNumber.singleNumber(new int[]{2, 2, 1}));
    }

    @Test
    void testCase2() {
        assertEquals(4, SingleNumber.singleNumber(new int[]{4, 1, 2, 1, 2}));
    }

    @Test
    void testCase3() {
        assertEquals(1, SingleNumber.singleNumber(new int[]{1}));
    }
}