package ru.rolsoft.leetcode.array;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class RotateArrayTest {

    @Test
    void rotateArray() {
        int[] expect = {6, 7, 8, 1, 2, 3, 4, 5};
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8};

        RotateArray.rotateArrayCopy(nums, 3);

        assertArrayEquals(expect, nums);
    }

    @Test
    void rotateArray2() {
        int[] expect = {1, 2, 3, 4, 5, 6, 7, 8};
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8};

        RotateArray.rotateArrayCopy(nums, 0);

        assertArrayEquals(expect, nums);
    }

    @Test
    void rotateArray3() {
        int[] expect = {6, 7, 8, 1, 2, 3, 4, 5};
        int[] nums = {1, 2, 3, 4, 5, 6, 7, 8};

        RotateArray.rotateArrayCopy(nums, 11);

        assertArrayEquals(expect, nums);
    }
}