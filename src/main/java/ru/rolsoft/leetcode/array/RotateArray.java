package ru.rolsoft.leetcode.array;

/**
 * Rotate Array
 * <p>
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/646/
 */
public class RotateArray {
    public static void rotateArrayCopy(int[] nums, int k) {
        if (k == 0) {
            return;
        }

        if (k > nums.length) {
            k = k % nums.length;
        }

        int[] extra = new int[k];

        System.arraycopy(nums, nums.length - k, extra, 0, k);

        System.arraycopy(nums, 0, nums, k, nums.length - k);

        System.arraycopy(extra, 0, nums, 0, k);
    }
}
