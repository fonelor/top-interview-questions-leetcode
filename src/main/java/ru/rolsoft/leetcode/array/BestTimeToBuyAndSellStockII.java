package ru.rolsoft.leetcode.array;

/**
 * Best Time to Buy and Sell Stock II
 * <p>
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/564/
 */
public class BestTimeToBuyAndSellStockII {
    public static int maxProfit(int[] prices) {
        // what is profit? is the difference between buy and sell price
        // when to buy? when the price will rise
        // when to sell? when the price will go down

        // if price monotonically rising - keep
        // if price drops even by one - sell on previous day

        int profit = 0;
        int buyPrice = 0;
        boolean inTransaction = false;
        for (int i = 0; i < prices.length; i++) {
            int currentPrice = prices[i];
            int nextPrice = i < prices.length - 1 ? prices[i + 1] : currentPrice;
            int cmp = Integer.compare(currentPrice, nextPrice);
            if (cmp > 0) {
                // if next value is less - sell
                if (inTransaction) {
                    profit += currentPrice - buyPrice;
                    inTransaction = false;
                }
            } else if (cmp < 0) {
                // if next value is higher - keep, or buy if we have nothing
                if (!inTransaction) {
                    buyPrice = currentPrice;
                    inTransaction = true;
                }
            } else if (i == prices.length - 1){
                // if equal - do nothing, except for last price
                if (inTransaction) {
                    profit += currentPrice - buyPrice;
                    inTransaction = false;
                }
            }
        }
        return profit;
    }
}
