package ru.rolsoft.leetcode.array;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Contains Duplicate
 * <p>
 * Given an integer array nums, return true if any value appears at least twice in the array, and return false if every element is distinct.
 * <p>
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/578/
 */
public class ContainsDuplicate {
    public static boolean containsDuplicateArray(int[] nums) {
        Arrays.sort(nums);
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == nums[i - 1]) {
                return true;
            }
        }
        return false;
    }

    public static boolean containsDuplicateSet(int[] nums) {
        HashSet<Integer> set = new HashSet<>();
        for (int value : nums) {
            if (!set.add(value)) {
                return true;
            }
        }
        return false;
    }
}
