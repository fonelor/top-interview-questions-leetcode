package ru.rolsoft.leetcode.array;

/**
 * Plus One
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/559/
 */
public class PlusOne {

    public static int[] plusOne(int[] digits) {
        // iterate from the last digit in digits array
        // if current number is less than 9 - increment and return from loop
        // if current number is 9 - write 0 and continue to the next digit
        boolean carryOver = false;
        for (int i = digits.length - 1; i >= 0; i--) {
            int digit = digits[i];
            carryOver = digit == 9;
            if (!carryOver) {
                digits[i] = digit + 1;
                break;
            } else {
                digits[i] = 0;
            }
        }

        if (carryOver) {
            int[] tmp = new int[digits.length + 1];
            tmp[0] = 1;
            System.arraycopy(digits, 0, tmp, 1, digits.length);
            digits = tmp;
        }

        return digits;
    }
}
