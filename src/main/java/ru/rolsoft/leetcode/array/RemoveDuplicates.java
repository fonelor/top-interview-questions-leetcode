package ru.rolsoft.leetcode.array;

/**
 * Remove Duplicates from Sorted Array
 * <p>
 * https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/727/
 */
public class RemoveDuplicates {
    public static int removeDuplicates(int[] nums) {
        // set lastValue to the first num
        // iterate over nums
        // if current element in nums equals to lastValue - remove
        // else move on
        if (nums.length < 1) {
            return 0;
        }

            int lastValueIndx = 1;
        int lastValue = nums[0];
        for (int i = 0; i < nums.length; i++) {
            int c = nums[i];
            if (c != lastValue) {
                nums[lastValueIndx++] = nums[i];
                lastValue = nums[i];
            }
        }
        return lastValueIndx;
    }
}
