package ru.rolsoft.leetcode.array;

/**
 * Move Zeroes
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/567/
 */
public class MoveZeroes {
    public static void moveZeroes(int[] nums) {
        // [ 1, 2, 3, 0, 4, 5, 6, 0]
        // increment zeroes counter
        // when first zero fond store the position in start variable
        // scan for the next zero
        // when next found - just copy array from start +1 to current position to start position
        // set start to the newly found zero position

        // when come to the end and start is not zero - copy array from start +1 to the end on start position
        // add to the end number of zeroes according to counter

        int zerosCnt = 0;
        int lastZeroPosition = -1;
        for (int pos = 0; pos < nums.length; pos++) {
            int current = nums[pos];
            if (current == 0) {
                if (lastZeroPosition >= 0) {
                    copyWithReplace(nums, lastZeroPosition, pos, zerosCnt);
                }
                zerosCnt++;
                lastZeroPosition = pos;
            }
        }

        if (lastZeroPosition >= 0) {
            copyWithReplace(nums, lastZeroPosition, nums.length, zerosCnt);
        }

        // add zeroes
        for (int i = 0; i < zerosCnt; i++) {
            nums[nums.length - zerosCnt + i] = 0;
        }
    }

    private static void copyWithReplace(int[] nums, int start, int pos, int zeros) {
        System.arraycopy(nums, start + 1,
                nums,
                start + 1 - zeros,
                pos - start - 1);
    }
}
