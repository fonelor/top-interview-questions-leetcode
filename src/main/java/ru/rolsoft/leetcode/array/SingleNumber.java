package ru.rolsoft.leetcode.array;

import java.util.Arrays;

/**
 * https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/549/
 */
public class SingleNumber {
    public static int singleNumber(int[] nums) {
        // sort  - logn
        Arrays.sort(nums);
        // iterate over nums and find the unique number
        for (int i = 0; i < nums.length - 1; i += 2) {
            if (nums[i] != nums[i + 1]) {
                return nums[i];
            }
        }
        return nums[nums.length - 1];
    }
}
