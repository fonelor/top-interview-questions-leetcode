package ru.rolsoft.leetcode.array;

import java.util.Arrays;
import java.util.HashMap;

/**
 * https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/674/
 */
public class IntersectionOfTwoArraysII {
    public static int[] intersect(int[] nums1, int[] nums2) {
        // create a map from one of the arrays  with keys as values and values as number of times value appears in the array
        HashMap<Integer, Integer> counts = new HashMap<>();
        for (int i : nums1) {
            counts.compute(i, (key, count) -> count == null ? 1 : count + 1);
        }

        // iterate over the second array and put each number that map contains to a list and decrease the number in map
        return Arrays.stream(nums2)
                .filter(n -> {
                    Integer count = counts.getOrDefault(n, 0);
                    if (count > 0) {
                        counts.put(n, --count);
                        return true;
                    }
                    return false;
                })
                .toArray();
    }
}
